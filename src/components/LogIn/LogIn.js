import React, {Component} from 'react';
import './logIn.scss'
import {reduxForm, Field} from 'redux-form';
import RenderField from "../common/RenderField/RenderField";
import {validate} from './validation'

import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSignInAlt} from '@fortawesome/free-solid-svg-icons'

library.add(faSignInAlt);

@reduxForm({
    form: 'logInForm',
    fields: ['email', 'password'],
    validate,
    enableReinitialize: true
})

class LogIn extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='card Login'>
                <div className='card-title'>Log In</div>
                <form  onSubmit={this.props.handleSubmit}>
                    <div className="form-group">
                        <Field
                            label="Email"
                            name='email'
                            component={RenderField}
                            type="email"
                        />
                    </div>
                    <div className="form-group">
                        <Field
                            label="Password"
                            name='password'
                            component={RenderField}
                            type="password"
                        />
                    </div>
                    <div className='d-flex flex-row-reverse'>
                        <button
                            className="btn btn-success"
                            type='submit'
                        >
                            Log In <FontAwesomeIcon icon='sign-in-alt'/>
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default LogIn

