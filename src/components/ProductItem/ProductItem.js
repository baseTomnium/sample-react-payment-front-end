import React, {Component} from 'react';
import './productItem.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'

library.add(faCartPlus);


class ProductItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {product, addToCart} = this.props;
        return (
            <div className="product-item">
                <div className='card content'>
                    <h5 className='card-title title'>{product.title}</h5>
                    <label className='caption'>Description:</label>
                    <p>{product.description}</p>
                    <label className='caption'>Price:</label>
                    <p>{product.price}₴</p>
                    <div className='d-flex flex-row-reverse'>
                        <button className='btn btn-success' onClick={()=>{addToCart(product._id)}}>
                            <FontAwesomeIcon icon='cart-plus'/>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductItem