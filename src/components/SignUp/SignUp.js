import React, {Component} from 'react';
import './signUp.scss'
import {validate} from './validation'
import {reduxForm, Field} from "redux-form";
import RenderField from "../common/RenderField/RenderField";

import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faUserCircle} from '@fortawesome/free-solid-svg-icons'

library.add(faUserCircle);

@reduxForm({
    form: 'signUp',
    fields: ['username','email', 'password'],
    validate,
    enableReinitialize: true
})
class SignUpComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='card Registration'>
                <div className='card-title'>Registration</div>
                    <form onSubmit={this.props.handleSubmit}>
                        <div className="form-group">
                            <Field
                                label="Username"
                                name='username'
                                component={RenderField}
                                type="text"
                            />
                        </div>
                        <div className="form-group">
                            <Field
                                label="Email"
                                name='email'
                                component={RenderField}
                                type="email"
                            />
                        </div>
                        <div className="form-group">
                            <Field
                                label="Password"
                                name='password'
                                component={RenderField}
                                type="password"
                            />
                        </div>
                        <div className='d-flex flex-row-reverse'>
                            <button
                                className="btn btn-success"
                                type='submit'
                            >
                                Sign Up <FontAwesomeIcon icon='user-circle'/>
                            </button>
                        </div>
                    </form>
            </div>
        )
    }
}

export default SignUpComponent