import React, {Component} from 'react';
import './cartItem.scss'
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTrashAlt} from '@fortawesome/free-solid-svg-icons'

library.add(faTrashAlt);

class CartItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {product, deleteFromCart} = this.props;
        return (
            <tr className='cart-row'>
                <td>{product.product.title}</td>
                <td>{product.product.description}</td>
                <td>{product.product.price}</td>
                <td>{product.quantity}</td>
                <td>
                    <button className='btn btn-outline-danger' style={{borderWidth:0}} onClick={() => {
                        deleteFromCart(product.product._id)
                    }}>
                        <FontAwesomeIcon icon='trash-alt'/>
                    </button>
                </td>
            </tr>
        )
    }
}

export default CartItem