import React, {Component} from 'react';
import './cartList.scss'
import CartItem from "./CartItem/CartItem";
import Checkout from "../Checkout/CheckoutComponent";
import {Link} from 'react-router-dom'

class CartList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {isLogin, products, totalPrice, deleteFromCart} = this.props;
        return (
            <div className="card cart-list">
                <div className='card-title'>Shopping cart:</div>
                <table className='table'>
                    <thead>
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col"/>
                    </tr>
                    </thead>
                    <tbody>
                    {products ? products.map((product, index) => {
                        return <CartItem key={index} product={product} deleteFromCart={deleteFromCart}/>
                    }) : null}
                    </tbody>
                </table>
                <p style={{fontWeight: 500}}>Total Price: {totalPrice}</p>
                <div className="d-flex flex-direction-row">
                    {isLogin ? <Checkout
                        name={'Payment'}
                        description={'Products Pay'}
                        amount={totalPrice}
                    /> : <div className='auth-alert'>
                        Login for payment
                        <div className="links">
                            <Link to='/signIn'>LogIn</Link> /
                            <Link to='/signUp'>Register</Link>
                        </div>
                    </div>}

                </div>
            </div>
        )
    }
}

export default CartList