import React, {Component} from 'react'
import {withRouter} from 'react-router';
import {connect} from "react-redux";
import {SignUpComponent} from "../components/index";
import {signUp} from "../redux/modules/auth.reducer";

@connect(
    ({auth, form}) => ({
        auth:auth,
        form:form
    }), {
        signUp
    }
)

class SignUpContainer extends Component {
    constructor(props) {
        super(props);
    }

    submit =(form)=>{
        this.props.signUp(form.username, form.email, form.password)
            .then(()=>{
                this.props.history.push('/signIn')
            }).catch((error)=>{
            alert(error.response.data.error)
        }
    );
    };

    render() {
        return (
        <div className="d-flex flex-column" style={{alignItems:'center'}}>
                <SignUpComponent onSubmit={this.submit}/>
            </div>
        )
    }
}

export default withRouter(SignUpContainer)